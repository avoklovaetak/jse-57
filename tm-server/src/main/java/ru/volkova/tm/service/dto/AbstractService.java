package ru.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.service.dto.IService;
import ru.volkova.tm.dto.AbstractEntity;

@Service
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
