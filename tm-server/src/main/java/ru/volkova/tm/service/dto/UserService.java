package ru.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.dto.IUserRepository;
import ru.volkova.tm.api.service.dto.IUserService;
import ru.volkova.tm.dto.User;

@Service
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    public IUserRepository getUserRepository() {
        return context.getBean(IUserRepository.class);
    }

    public void setPassword(
            @NotNull final String userId,
            @Nullable final String password
    ) {
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.setPassword(userId, password);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

}
