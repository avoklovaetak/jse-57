package ru.volkova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.model.ISessionGraphRepository;
import ru.volkova.tm.api.service.model.IAdminUserGraphService;
import ru.volkova.tm.api.service.model.ISessionGraphService;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.model.SessionGraph;
import ru.volkova.tm.model.UserGraph;
import ru.volkova.tm.util.HashUtil;
import ru.volkova.tm.api.service.IPropertyService;

import java.util.List;

@Service
public final class SessionGraphService extends AbstractService<SessionGraph> implements ISessionGraphService {

    @NotNull
    @Autowired
    private IAdminUserGraphService adminUserService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public void add(@NotNull SessionGraph sessionGraph) {
        @NotNull final ISessionGraphRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.add(sessionGraph);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final UserGraph userGraph = adminUserService.findByLogin(login);
        if (userGraph == null) return false;
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        final String passwordHash2 = userGraph.getPasswordHash();
        return passwordHash.equals(passwordHash2);
    }

    @Override
    @Nullable
    public SessionGraph close(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) return null;
        @NotNull final ISessionGraphRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.close(sessionGraph);
            sessionRepository.commit();
            return sessionGraph;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @Nullable
    public List<SessionGraph> findAll() {
        return null;
    }

    @NotNull
    public ISessionGraphRepository getSessionRepository() {
        return context.getBean(ISessionGraphRepository.class);
    }

    @Nullable
    @Override
    public SessionGraph open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @Nullable final UserGraph userGraph = adminUserService.findByLogin(login);
        if (userGraph == null) return null;
        @NotNull final SessionGraph sessionGraph = new SessionGraph();
        sessionGraph.setUser(userGraph);
        @Nullable final SessionGraph signSessionGraph = sign(sessionGraph);
        if (signSessionGraph == null) return null;
        @NotNull final ISessionGraphRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.add(sessionGraph);
            sessionRepository.commit();
            return sessionGraph;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Nullable
    public SessionGraph sign(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) return null;
        sessionGraph.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, sessionGraph);
        sessionGraph.setSignature(signature);
        return sessionGraph;
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionGraph sessionGraph) {
        if (sessionGraph == null) throw new AccessDeniedException();
        final String signature = sessionGraph.getSignature();
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException();
        if (sessionGraph.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionGraph temp = sessionGraph.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = sessionGraph.getSignature();
        @Nullable final SessionGraph sessionGraphTarget = sign(temp);
        if (sessionGraphTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionGraphTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final SessionGraph sessionGraph, @Nullable final Role role) {
        if (sessionGraph == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        validate(sessionGraph);
        final @Nullable UserGraph userGraph = adminUserService.findById(sessionGraph.getUser().getId());
        if (userGraph == null) throw new AccessDeniedException();
        if (userGraph.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

}
