package ru.volkova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.dto.IUserRepository;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.EmailExistsException;
import ru.volkova.tm.exception.auth.LoginExistsException;
import ru.volkova.tm.exception.empty.*;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.util.HashUtil;

import java.util.List;

@Service
public class AdminUserService extends AbstractService<User> implements IAdminUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @NotNull
    public User add(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.insert(user);
            userRepository.commit();
            return user;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<User> entities) {
        if (entities == null) throw new UserNotFoundException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            entities.forEach(userRepository::insert);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.clear();
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @NotNull
    @Override
    public User createUser(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User createUserWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        User user = new User();
        user.setEmail(email);
        final String passwordHash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(passwordHash);
        user.setLogin(login);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public User createUserWithRole
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        User user = new User();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setLogin(login);
        user.setRole(role);
        add(user);
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final List<User> users = userRepository.findAll();
            userRepository.commit();
            return users;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final User user = userRepository.findByEmail(email);
            userRepository.commit();
            return user;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final User user = userRepository.findById(id);
            userRepository.commit();
            return user;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            final User user = userRepository.findByLogin(login);
            userRepository.commit();
            return user;
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @NotNull
    public IUserRepository getUserRepository() {
        return context.getBean(IUserRepository.class);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        @Nullable final User user = findByEmail(email);
        return user != null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        @Nullable final User user = findByLogin(login);
        return user != null;
    }

    @Override
    public void lockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.lockByEmail(email);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void lockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.lockById(id);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void lockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.lockByLogin(login);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void removeByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.removeByEmail(email);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.removeById(id);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.removeByLogin(login);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }


    @Override
    public void unlockByEmail(@Nullable String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.unlockByEmail(email);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void unlockById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.unlockById(id);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    @Override
    public void unlockByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.unlockByLogin(login);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    ) {
        @Nullable final User user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        if (firstName == null || firstName.isEmpty()
                || secondName == null || secondName.isEmpty()
                || middleName == null || middleName.isEmpty())
            throw new UserNotFoundException();
        @NotNull final IUserRepository userRepository = getUserRepository();
        try {
            userRepository.begin();
            userRepository.updateUser(userId, firstName, secondName, middleName);
            userRepository.commit();
        } catch (@NotNull final Exception e) {
            userRepository.rollback();
            throw e;
        } finally {
            userRepository.close();
        }
    }

}

