package ru.volkova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.dto.ITaskRepository;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.api.service.dto.ITaskService;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.enumerated.Status;
import ru.volkova.tm.exception.empty.EmptyNameException;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;

@Service
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    @Autowired
    private IAdminUserService adminUserService;

    @Override
    public void add(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new ObjectNotFoundException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        task.setUser(adminUserService.findById(userId));
        try {
            taskRepository.begin();
            taskRepository.insert(task);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<Task> entities) {
        if (entities == null) throw new TaskNotFoundException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            entities.forEach(taskRepository::insert);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void changeOneStatusById(
            @NotNull String userId, @NotNull String id, @Nullable Status status
    ) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.changeOneStatusById(userId, id, status);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void changeOneStatusByName(
            @NotNull String userId, @NotNull String name, @Nullable Status status
    ) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.changeOneStatusByName(userId, name, status);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.clear(userId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final List<Task> tasks = taskRepository.findAll(userId);
            taskRepository.commit();
            return tasks;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    public Task findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final Task task = taskRepository.findById(userId, id);
            taskRepository.commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index < 0) return null;
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final Task task = taskRepository.findOneByIndex(userId, index);
            taskRepository.commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull String userId, @NotNull String name) {
        if (name.isEmpty()) return null;
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final Task task = taskRepository.findOneByName(userId, name);
            taskRepository.commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    public ITaskRepository getTaskRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @Override
    public Task insert(@NotNull final Task task) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.insert(task);
            taskRepository.commit();
            return task;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void removeById(@NotNull String userId, @NotNull String id) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.removeById(userId, id);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void removeOneByName(@NotNull String userId, @NotNull String name) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.removeOneByName(userId, name);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void updateOneById(
            @NotNull String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.updateOneById(userId, id, name, description);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}
