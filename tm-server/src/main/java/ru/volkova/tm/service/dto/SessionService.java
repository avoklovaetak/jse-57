package ru.volkova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.dto.ISessionRepository;
import ru.volkova.tm.api.service.IPropertyService;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.exception.auth.AccessDeniedException;
import ru.volkova.tm.util.HashUtil;

import java.util.List;

@Service
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    @Autowired
    private IAdminUserService adminUserService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    public void add(@NotNull Session session) {
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.add(session);
            sessionRepository.commit();
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        @Nullable final User user = adminUserService.findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        final String passwordHash2 = user.getPasswordHash();
        return passwordHash.equals(passwordHash2);
    }

    @Override
    @Nullable
    public Session close(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        try {
            sessionRepository.begin();
            sessionRepository.close(session);
            sessionRepository.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Override
    @Nullable
    public List<Session> findAll() {
        return null;
    }

    @NotNull
    public ISessionRepository getSessionRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @Nullable
    @Override
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        @NotNull final ISessionRepository sessionRepository = getSessionRepository();
        @Nullable final User user = adminUserService.findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUser(user);
        @Nullable final Session signSession = sign(session);
        if (signSession == null) return null;
        try {
            sessionRepository.begin();
            sessionRepository.add(session);
            sessionRepository.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sessionRepository.rollback();
            throw e;
        } finally {
            sessionRepository.close();
        }
    }

    @Nullable
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.salt(propertyService, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        final String signature = session.getSignature();
        if (signature == null || signature.isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionTarget = sign(temp);
        if (sessionTarget == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionTarget.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    @Override
    @SneakyThrows
    public void validateAdmin(@Nullable final Session session, @Nullable final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        validate(session);
        final @Nullable User user = adminUserService.findById(session.getUser().getId());
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() != Role.ADMIN) throw new AccessDeniedException();
    }

}
