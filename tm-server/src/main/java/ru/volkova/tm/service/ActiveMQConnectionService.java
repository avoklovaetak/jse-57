package ru.volkova.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.service.IActiveMQConnectionService;

import javax.jms.*;

@Getter
public class ActiveMQConnectionService implements IActiveMQConnectionService {

    @NotNull
    public static final String JMS_LOGGER_TOPIC = "JCG_TOPIC";

    @NotNull
    private static final String JMS_BROKER_URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static ActiveMQConnectionService instance;

    @NotNull
    private final MessageService messageService;

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(JMS_BROKER_URL);

    @NotNull
    private final Connection connection;

    @SneakyThrows
    public ActiveMQConnectionService() {
        messageService = new MessageService();
        connection = connectionFactory.createConnection();
        connection.start();
        instance = this;
    }

    public static ActiveMQConnectionService getInstance() {
        return instance;
    }

    @Override
    @SneakyThrows
    public void shutDown() {
        connection.close();
    }

}
