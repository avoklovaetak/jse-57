package ru.volkova.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.volkova.tm.api.repository.dto.IProjectRepository;
import ru.volkova.tm.api.repository.dto.ITaskRepository;
import ru.volkova.tm.api.service.dto.IProjectTaskService;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.exception.entity.ProjectNotFoundException;
import ru.volkova.tm.exception.entity.TaskNotFoundException;

import java.util.List;

@Service
public class ProjectTaskService extends AbstractService<Task> implements IProjectTaskService {

    @Override
    public void bindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.bindTaskByProjectId(userId, projectId, taskId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
            taskRepository.commit();
            return tasks;
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @NotNull
    public IProjectRepository getProjectRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    public ITaskRepository getTaskRepository() {
        return context.getBean(ITaskRepository.class);
    }

    @Override
    public void removeProjectById(
            @NotNull final String userId,
            @Nullable final String projectId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        @NotNull final IProjectRepository projectRepository = getProjectRepository();
        try {
            taskRepository.begin();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

    @Override
    public void unbindTaskByProjectId(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final ITaskRepository taskRepository = getTaskRepository();
        try {
            taskRepository.begin();
            taskRepository.unbindTaskByProjectId(userId, projectId, taskId);
            taskRepository.commit();
        } catch (@NotNull final Exception e) {
            taskRepository.rollback();
            throw e;
        } finally {
            taskRepository.close();
        }
    }

}
