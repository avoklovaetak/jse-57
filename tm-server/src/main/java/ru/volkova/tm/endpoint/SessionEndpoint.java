package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.api.endpoint.ISessionEndpoint;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @Override
    @Nullable
    @WebMethod
    public Session closeSession(
            @WebParam(name = "session", partName = "session") @NotNull final Session session
    ) {
        return sessionService.close(session);
    }

    @Override
    @WebMethod
    public Session openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        return sessionService.open(login, password);
    }

}

