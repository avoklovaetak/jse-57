package ru.volkova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.api.endpoint.IProjectEndpoint;
import ru.volkova.tm.api.service.dto.IProjectService;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.dto.Project;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @WebMethod
    public void addProjectByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectService.add(userId, name, description);
    }

    @SneakyThrows
    @WebMethod
    public void changeProjectOneStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        sessionService.validate(session);
        projectService
                .changeOneStatusById(session.getUser().getId(), id, status);
    }

    @SneakyThrows
    @WebMethod
    public void changeProjectStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        sessionService.validate(session);
        projectService
                .changeOneStatusByName(session.getUser().getId(), name, status);
    }

    @WebMethod
    public void clearProject(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectService.clear(userId);
    }

    @SneakyThrows
    @NotNull
    @WebMethod
    public List<Project> findAllProjects(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return projectService.findAll(userId);
    }

    @Nullable
    @WebMethod
    public Project findProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return projectService.findById(userId, id);
    }

    @Nullable
    @WebMethod
    public Project findProjectByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return projectService.findOneByIndex(userId, index);
    }

    @Nullable
    @WebMethod
    public Project findProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return projectService.findOneByName(userId, name);
    }

    @WebMethod
    public void removeProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectService.removeById(userId, id);
    }

    @WebMethod
    public void removeProjectByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectService.removeOneByName(userId, name);
    }

    @WebMethod
    public void updateProjectById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        projectService
                .updateOneById(userId, id, name, description);
    }

}
