package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.api.endpoint.IUserEndpoint;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.api.service.dto.IUserService;
import ru.volkova.tm.dto.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Component
@WebService
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @WebMethod
    public void setPassword(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        userService.setPassword(userId, password);
    }

}
