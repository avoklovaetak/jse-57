package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.api.endpoint.IAdminUserEndpoint;
import ru.volkova.tm.api.service.dto.IAdminUserService;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.User;
import ru.volkova.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IAdminUserService adminUserService;

    @WebMethod
    public void addUser(
            @Nullable @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") User entity
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.add(entity);
    }

    @WebMethod
    public void clearUsers(@NotNull @WebParam(name = "session", partName = "session") Session session) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.clear();
    }

    @WebMethod
    public void createUserByLogPass(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.createUser(login, password);
    }

    @WebMethod
    public void createUserWithEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.createUserWithEmail(login, password, email);
    }

    @WebMethod
    public void createUserWithRole(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password,
            @Nullable @WebParam(name = "role", partName = "role") Role role
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.createUserWithRole(login, password, role);
    }

    @NotNull
    @WebMethod
    public List<User> findAllUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        return adminUserService.findAll();
    }

    @Nullable
    @WebMethod
    public User findUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        return adminUserService.findById(id);
    }

    @Nullable
    @WebMethod
    public User findUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        return adminUserService.findByLogin(login);
    }

    @WebMethod
    public boolean isEmailExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        return adminUserService.isEmailExists(email);
    }

    @WebMethod
    public boolean isLoginExists(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        return adminUserService.isLoginExists(login);
    }

    @WebMethod
    public void lockByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.lockByEmail(email);
    }

    @WebMethod
    public void lockById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.lockById(id);
    }

    @WebMethod
    public void lockByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.lockByLogin(login);
    }

    @WebMethod
    public void removeUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.removeByEmail(email);
    }

    @WebMethod
    public void removeUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.removeById(id);
    }

    @WebMethod
    public void removeUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.removeByLogin(login);
    }


    @WebMethod
    public void unlockUserByEmail(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "email", partName = "email") String email
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.unlockByEmail(email);
    }

    @WebMethod
    public void unlockUserById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.unlockById(id);
    }

    @WebMethod
    public void unlockUserByLogin(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        adminUserService.unlockByLogin(login);
    }

    @WebMethod
    public void updateUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "firstName", partName = "firstName") String firstName,
            @Nullable @WebParam(name = "secondName", partName = "secondName") String secondName,
            @Nullable @WebParam(name = "middleName", partName = "middleName") String middleName
    ) {
        sessionService.validateAdmin(session, Role.ADMIN);
        final String userId = session.getUser().getId();
        adminUserService
                .updateUser(userId, firstName, secondName, middleName);
    }

}
