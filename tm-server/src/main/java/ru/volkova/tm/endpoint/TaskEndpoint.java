package ru.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.volkova.tm.api.service.dto.ISessionService;
import ru.volkova.tm.api.service.dto.ITaskService;
import ru.volkova.tm.dto.Session;
import ru.volkova.tm.dto.Task;
import ru.volkova.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private ITaskService taskService;

    @Nullable
    @WebMethod
    public Task addTask(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "entity", partName = "entity") Task entity
    ) {
        sessionService.validate(session);
        return taskService.insert(entity);
    }

    @WebMethod
    public void addTaskByUser(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        taskService.add(userId, name, description);
    }

    @WebMethod
    public void changeTaskStatusById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "status", partName = "id") Status status
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        taskService.changeOneStatusById(userId, id, status);
    }

    @WebMethod
    public void changeTaskStatusByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "status", partName = "status") Status status
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        taskService
                .changeOneStatusByName(userId, name, status);
    }

    @WebMethod
    public void clearTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        taskService.clear(userId);
    }

    @NotNull
    @WebMethod
    public List<Task> findAllTasks(
            @NotNull @WebParam(name = "session", partName = "session") Session session
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return taskService.findAll(userId);
    }

    @Nullable
    @WebMethod
    public Task findTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return taskService.findById(userId, id);
    }

    @Nullable
    @WebMethod
    public Task findTaskByIndex(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "index", partName = "index") Integer index
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return taskService
                .findOneByIndex(userId, index);
    }

    @Nullable
    @WebMethod
    public Task findTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        return taskService.findOneByName(userId, name);
    }

    @WebMethod
    public void removeTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @NotNull @WebParam(name = "id", partName = "id") String id
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        taskService.removeById(userId, id);
    }

    @WebMethod
    public void removeTaskByName(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        taskService.removeOneByName(userId, name);
    }

    @WebMethod
    public void updateTaskById(
            @NotNull @WebParam(name = "session", partName = "session") Session session,
            @Nullable @WebParam(name = "id", partName = "id") String id,
            @Nullable @WebParam(name = "name", partName = "name") String name,
            @Nullable @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        final String userId = session.getUser().getId();
        taskService
                .updateOneById(userId, id, name, description);
    }

}
