package ru.volkova.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.volkova.tm.api.repository.model.IUserGraphRepository;
import ru.volkova.tm.exception.entity.UserNotFoundException;
import ru.volkova.tm.model.TaskGraph;
import ru.volkova.tm.model.UserGraph;

import java.util.List;

@Repository
@Scope("prototype")
public class UserGraphRepository extends AbstractRepository<UserGraph> implements IUserGraphRepository {

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM user t", UserGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true).executeUpdate();
    }

    @Override
    @NotNull
    public List<UserGraph> findAll() {
        return entityManager
                .createQuery("SELECT t FROM user t", UserGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Override
    @Nullable
    public UserGraph findByEmail(@NotNull String email) {
        return entityManager
                .createQuery("SELECT t FROM user t WHERE t.email = :email", UserGraph.class)
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public UserGraph findById(@NotNull String id) {
        return entityManager
                .createQuery("SELECT t FROM user t WHERE t.id = :id", UserGraph.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @Nullable
    public UserGraph findByLogin(@NotNull String login) {
        return entityManager
                .createQuery("SELECT t FROM user t WHERE t.login = :login", UserGraph.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    @NotNull
    public UserGraph insert(@Nullable UserGraph userGraph) {
        if (userGraph == null) throw new UserNotFoundException();
        entityManager.persist(userGraph);
        return userGraph;
    }

    @Override
    public void lockByEmail(@NotNull String email) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.email = :email", UserGraph.class)
                .setParameter("email", email)
                .setParameter("locked", false)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void lockById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.id = :id", UserGraph.class)
                .setParameter("id", id)
                .setParameter("locked", false)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void lockByLogin(@NotNull String login) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.login = :login", UserGraph.class)
                .setParameter("login", login)
                .setParameter("locked", false)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void removeByEmail(@NotNull String email) {
        entityManager
                .createQuery("DELETE FROM user t WHERE t.email = :email",
                        UserGraph.class)
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void removeById(@NotNull String id) {
        entityManager
                .createQuery("DELETE FROM user t WHERE t.id = :id",
                        UserGraph.class)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void removeByLogin(@NotNull String login) {
        entityManager
                .createQuery("DELETE FROM user t WHERE t.login = :login",
                        UserGraph.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

    @Override
    public void setPassword(@NotNull String userId, @Nullable String hash) {
        entityManager
                .createQuery("UPDATE user t SET t.password_hash = :passwordHash" +
                        "WHERE t.id = :id", UserGraph.class)
                .setParameter("passwordHash", hash)
                .setParameter("id", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void unlockByEmail(@NotNull String email) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.email = :email", UserGraph.class)
                .setParameter("email", email)
                .setParameter("locked", true)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void unlockById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.id = :id", UserGraph.class)
                .setParameter("id", id)
                .setParameter("locked", true)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void unlockByLogin(@NotNull String login) {
        entityManager
                .createQuery("UPDATE user t SET t.locked = :locked" +
                        "WHERE t.login = :login", UserGraph.class)
                .setParameter("login", login)
                .setParameter("locked", true)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

    @Override
    public void updateUser(
            @NotNull String userId,
            @Nullable String firstName,
            @Nullable String secondName,
            @Nullable String middleName
    ) {
        entityManager
                .createQuery("UPDATE user t SET t.first_name = :firstName, " +
                        "t.second_name = :secondName, t.middle_name = :middleName" +
                        "WHERE t.id = :id", TaskGraph.class)
                .setParameter("firstName", firstName)
                .setParameter("secondName", secondName)
                .setParameter("middleName", middleName)
                .setParameter("id", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .executeUpdate();
    }

}
