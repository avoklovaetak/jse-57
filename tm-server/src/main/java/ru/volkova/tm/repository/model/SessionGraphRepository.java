package ru.volkova.tm.repository.model;

import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.volkova.tm.api.repository.model.ISessionGraphRepository;
import ru.volkova.tm.model.SessionGraph;

@Repository
@Scope("prototype")
public class SessionGraphRepository extends AbstractRepository<SessionGraph> implements ISessionGraphRepository {

    @Override
    public void add(@NotNull SessionGraph sessionGraph) {
        entityManager.persist(sessionGraph);
    }

    @Override
    public void close(@NotNull SessionGraph sessionGraph) {
        entityManager.createQuery("DELETE t FROM session t", SessionGraph.class)
                .setHint(QueryHints.HINT_CACHEABLE, true);
    }

}
