package ru.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.model.AbstractEntityGraph;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public abstract class AbstractRepository<E extends AbstractEntityGraph> implements IRepositoryGraph<E> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void begin() {
        entityManager.getTransaction().begin();
    }

    @Override
    public void close() {
        entityManager.close();
    }

    @Override
    public void commit() {
        entityManager.getTransaction().commit();
    }

    @Override
    public void rollback() {
        entityManager.getTransaction().rollback();
    }

}
