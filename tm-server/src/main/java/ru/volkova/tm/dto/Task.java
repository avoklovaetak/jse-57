package ru.volkova.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.entity.IWBS;
import ru.volkova.tm.listener.EntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@EntityListeners(EntityListener.class)
public class Task extends AbstractOwnerEntity implements IWBS {

    @Nullable
    @ManyToOne
    private Project project;

    @NotNull
    public String toString() {
        return id + ": " + getName() + ": " + project.getId()
                + "; " + "created: " + getCreated() +
                "started: " + getDateStart();
    }

}
