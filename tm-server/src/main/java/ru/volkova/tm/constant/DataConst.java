package ru.volkova.tm.constant;

public interface DataConst {

    String FILE_JSON = "./backup.json";

    String FILE_BINARY = "./data.bin";

    String FILE_BASE64 = "./data.base64";

    String FILE_JAXB_XML = "./data-jaxb.xml";

    String FILE_JAXB_JSON = "./data-jaxb.json";

    String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    String JAXB_JSON_PROPERTY_NAME = "eclipselink.media-type";

    String JAXB_JSON_PROPERTY_VALUE = "application/json";

    String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";

    String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";

}
