package ru.volkova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepository;
import ru.volkova.tm.dto.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    void clear();

    @NotNull
    List<User> findAll();

    @Nullable
    User findByEmail(@NotNull String email);

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @NotNull
    User insert(@Nullable User user);

    void lockByEmail(@NotNull String email);

    void lockById(@NotNull String id);

    void lockByLogin(@NotNull String login);

    void removeByEmail(@NotNull String email);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

    void setPassword(
            @NotNull final String userId,
            @Nullable final String hash
    );

    void unlockByEmail(@NotNull String email);

    void unlockById(@NotNull String id);

    void unlockByLogin(@NotNull String login);

    void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

}
