package ru.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.model.UserGraph;

public interface IUserGraphService extends IServiceGraph<UserGraph> {

    void setPassword(@NotNull String userId, @Nullable String password);

}
