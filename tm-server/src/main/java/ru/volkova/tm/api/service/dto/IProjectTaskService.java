package ru.volkova.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.dto.Task;

import java.util.List;

public interface IProjectTaskService extends IService<Task> {

    void bindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @NotNull String taskId
    );

    @NotNull
    List<Task> findAllTasksByProjectId(@NotNull String userId, @Nullable String projectId);

    void removeProjectById(@NotNull String userId, @Nullable String id);

    void unbindTaskByProjectId(
            @NotNull String userId,
            @Nullable String projectId,
            @NotNull String taskId
    );

}
