package ru.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.model.UserGraph;


import java.util.List;

public interface IAdminUserGraphService extends IServiceGraph<UserGraph> {

    @NotNull
    UserGraph add(@Nullable final UserGraph userGraph);

    void addAll(@Nullable List<UserGraph> entities);

    void clear();

    @NotNull
    UserGraph createUser(@Nullable final String login, @Nullable final String password);

    @NotNull
    UserGraph createUserWithEmail(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    @NotNull
    UserGraph createUserWithRole
            (@Nullable final String login,
             @Nullable final String password,
             @Nullable final Role role);

    @NotNull
    List<UserGraph> findAll();

    @Nullable
    UserGraph findByEmail(@Nullable final String email);

    @Nullable
    UserGraph findById(@Nullable final String id);

    @Nullable
    UserGraph findByLogin(@Nullable final String login);

    boolean isEmailExists(@Nullable final String email);

    boolean isLoginExists(@Nullable final String login);

    void lockByEmail(@Nullable String email);

    void lockById(@Nullable String id);

    void lockByLogin(@Nullable String login);

    void removeByEmail(@Nullable String email);

    void removeById(@Nullable final String id);

    void removeByLogin(@Nullable final String login);

    void unlockByEmail(@Nullable String email);

    void unlockById(@Nullable String id);

    void unlockByLogin(@Nullable String login);

    void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String secondName,
            @Nullable final String middleName
    );

}
