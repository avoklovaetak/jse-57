package ru.volkova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.enumerated.Role;
import ru.volkova.tm.model.SessionGraph;

import java.util.List;

public interface ISessionGraphService extends IServiceGraph<SessionGraph> {

    void add(@NotNull SessionGraph sessionGraph);

    boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    );

    @Nullable
    SessionGraph close(@Nullable SessionGraph sessionGraph);

    @Nullable
    List<SessionGraph> findAll();

    @Nullable
    SessionGraph open(String login, String password);

    void validate(@Nullable final SessionGraph sessionGraph);

    void validateAdmin(@Nullable final SessionGraph sessionGraph, @Nullable final Role role);

}
