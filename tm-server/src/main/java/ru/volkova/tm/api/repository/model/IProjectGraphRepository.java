package ru.volkova.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.api.repository.IRepositoryGraph;
import ru.volkova.tm.model.ProjectGraph;
import ru.volkova.tm.enumerated.Status;

import java.util.List;

public interface IProjectGraphRepository extends IRepositoryGraph<ProjectGraph> {

    void changeOneStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    );

    void changeOneStatusByName(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final Status status
    );

    void clear(@NotNull String userId);

    @NotNull
    List<ProjectGraph> findAll(@NotNull String userId);

    @Nullable
    ProjectGraph findById(@NotNull final String userId, @NotNull final String id);

    @Nullable
    ProjectGraph findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    ProjectGraph findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    ProjectGraph insert(@Nullable final ProjectGraph projectGraph);

    void removeById(@NotNull String userId, @NotNull String id);

    void removeOneByName(@NotNull String userId, @Nullable String name);

    void updateOneById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
