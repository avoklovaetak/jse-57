package ru.volkova.tm.event;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public final class ConsoleEvent {

    @NotNull
    private final String name;

    public ConsoleEvent(@NotNull String name) {
        this.name = name;
    }

}
