package ru.volkova.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.volkova.tm.bootstrap.Bootstrap;
import ru.volkova.tm.configuration.ClientConfiguration;

public class Application {

    public static void main(@NotNull String[] args) throws Exception {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ClientConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
