package ru.volkova.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Status;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "change project status by id";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectChangeStatusByIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID");
        @Nullable final Session session = sessionService.getSession();
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusId = TerminalUtil.nextLine();
        @Nullable final Status status = Status.valueOf(statusId);
        projectEndpoint.changeProjectOneStatusById(session, id, status);
    }

    @NotNull
    @Override
    public String name() {
        return "project-change-status-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
