package ru.volkova.tm.listener.bonds;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

@Component
public class ProjectCascadeRemoveByIdListener extends AbstractProjectTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "remove project and all project tasks by id";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectCascadeRemoveByIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[REMOVE PROJECT AND ALL TASKS CASCADE]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Session session = sessionService.getSession();
        if (session == null) throw new ObjectNotFoundException();
        projectTaskEndpoint.removeProjectById(session, projectId);
    }

    @NotNull
    @Override
    public String name() {
        return "project-cascade-remove-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
