package ru.volkova.tm.listener.authorization;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

@Component
public class UserLogoutListener extends AbstractAuthListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "log out of system";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userLogoutListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSession(session);
    }

    @NotNull
    @Override
    public String name() {
        return "user-logout";
    }

}
