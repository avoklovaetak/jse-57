package ru.volkova.tm.listener.system;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.listener.AbstractListener;

public class AboutListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show developer info";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@aboutListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println("NAME: " + Manifests.read("developer"));
        System.out.println("E-MAIL: " + Manifests.read("email"));
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

}
