package ru.volkova.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.listener.AbstractListener;
import ru.volkova.tm.util.NumberUtil;

public class SystemInfoListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show system info";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@systemInfoListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[SYSTEM INFO]");
        final long availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long memoryTotal = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.format(memoryTotal));
        final long usedMemory = memoryTotal - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }

    @NotNull
    @Override
    public String name() {
        return "system-info";
    }

}
