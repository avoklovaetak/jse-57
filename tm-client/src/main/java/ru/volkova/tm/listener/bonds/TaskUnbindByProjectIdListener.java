package ru.volkova.tm.listener.bonds;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

@Component
public class TaskUnbindByProjectIdListener extends AbstractProjectTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "unbind task out of project";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskUnbindByProjectIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @Nullable final Session session = sessionService.getSession();
        if (session == null) throw new ObjectNotFoundException();
        projectTaskEndpoint.unbindTaskByProjectId(session, projectId, taskId);
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
