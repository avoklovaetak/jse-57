package ru.volkova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.endpoint.Project;
import ru.volkova.tm.endpoint.ProjectEndpoint;
import ru.volkova.tm.listener.AbstractListener;

public abstract class AbstractProjectListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ProjectEndpoint projectEndpoint;

    protected void showProject(@NotNull final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + project.getStatus());
    }

}
