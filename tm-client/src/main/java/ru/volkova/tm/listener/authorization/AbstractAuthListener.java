package ru.volkova.tm.listener.authorization;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.endpoint.AdminUserEndpoint;
import ru.volkova.tm.endpoint.SessionEndpoint;
import ru.volkova.tm.endpoint.UserEndpoint;
import ru.volkova.tm.listener.AbstractListener;

public abstract class AbstractAuthListener extends AbstractListener {

    @NotNull
    @Autowired
    public SessionEndpoint sessionEndpoint;

    @NotNull
    @Autowired
    public UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public AdminUserEndpoint adminUserEndpoint;

}
