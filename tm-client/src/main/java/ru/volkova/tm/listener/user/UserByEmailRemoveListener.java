package ru.volkova.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

@Component
public class UserByEmailRemoveListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "remove user by email";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userByEmailRemoveListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        adminEndpoint.removeUserByEmail(session, email);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-email";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
