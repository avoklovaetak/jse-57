package ru.volkova.tm.listener.bonds;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Task;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TasksShowByProjectIdListener extends AbstractProjectTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show all tasks of project by project id";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskShowByProjectIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[TASK LIST OF PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final List<Task> tasks = projectTaskEndpoint
                .findAllTasksByProjectId(session, projectId);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println((index + ". " + task));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "tasks-show-by-project-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}

