package ru.volkova.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

@Component
public class UserByIdUnlockListener extends AbstractUserListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "unlock user by id";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userByIdUnlockListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        adminEndpoint.unlockUserById(session, id);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-id";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
