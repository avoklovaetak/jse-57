package ru.volkova.tm.listener.system;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.listener.AbstractListener;

public class VersionListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show application version";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@versionListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(Manifests.read("build"));

    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

}
