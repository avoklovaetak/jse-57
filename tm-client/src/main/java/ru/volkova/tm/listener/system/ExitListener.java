package ru.volkova.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.listener.AbstractListener;

public class ExitListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "close application";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@exitListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    @NotNull
    @Override
    public String name() {
        return "exit";
    }

}
