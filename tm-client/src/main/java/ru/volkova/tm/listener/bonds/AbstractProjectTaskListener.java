package ru.volkova.tm.listener.bonds;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.endpoint.ProjectTaskEndpoint;
import ru.volkova.tm.listener.AbstractListener;

public abstract class AbstractProjectTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ProjectTaskEndpoint projectTaskEndpoint;

}
