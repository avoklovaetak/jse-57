package ru.volkova.tm.listener.authorization;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.User;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

@Component
public class UserViewProfileListener extends AbstractAuthListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "view user profile";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userViewProfileListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[VIEW PROFILE]");
        final Session session = sessionService.getSession();
        if (session == null) throw new ObjectNotFoundException();
        final String userId = session.getUser().getId();
        @NotNull final User user = adminUserEndpoint.findUserById(session, userId);
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME" + user.getMiddleName());
    }

    @Override
    public String name() {
        return "user-view-profile";
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}
