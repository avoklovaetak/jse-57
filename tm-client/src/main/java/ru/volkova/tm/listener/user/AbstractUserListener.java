package ru.volkova.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.listener.AbstractListener;
import ru.volkova.tm.endpoint.AdminUserEndpoint;

public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    public AdminUserEndpoint adminEndpoint;

}
