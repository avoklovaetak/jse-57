package ru.volkova.tm.listener.authorization;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

@Component
public class UserUpdateProfileListener extends AbstractAuthListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "update information about user";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userUpdateProfileListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable Session session = sessionService.getSession();
        if (session == null) throw new ObjectNotFoundException();
        System.out.println("[UPDATE PROFILE]");
        System.out.println("ENTER FIRST NAME: ");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER LAST NAME: ");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER MIDDLE NAME: ");
        @NotNull final String middleName = TerminalUtil.nextLine();
        adminUserEndpoint.updateUser(session, firstName, lastName, middleName);
    }

    @NotNull
    @Override
    public String name() {
        return "user-update-profile";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
