package ru.volkova.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.listener.AbstractListener;

public class CommandsListListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show program commands";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@commandsListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        for (@NotNull final AbstractListener listener : listeners) {
            if (listener.name() != null) {
                System.out.println("[" + listener.name() + "] - " + listener.description());
            }
        }
    }

    @NotNull
    @Override
    public String name() {
        return "commands-list";
    }

}
