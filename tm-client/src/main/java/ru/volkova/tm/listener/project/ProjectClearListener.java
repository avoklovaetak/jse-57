package ru.volkova.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;

@Component
public class ProjectClearListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "clear all projects";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectClearListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[PROJECT CLEAR]");
        @Nullable final Session session = sessionService.getSession();
        projectEndpoint.clearProject(session);
    }

    @NotNull
    @Override
    public String name() {
        return "project-clear";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
