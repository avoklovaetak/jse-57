package ru.volkova.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByIdListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "remove project by name";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectRemoveByIdListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        projectEndpoint.removeProjectById(session, id);
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
