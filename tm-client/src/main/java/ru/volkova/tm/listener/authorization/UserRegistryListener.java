package ru.volkova.tm.listener.authorization;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

@Component
public class UserRegistryListener extends AbstractAuthListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "registry new user in system";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@userRegistryListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[REGISTRATION]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();
        @Nullable final Session session = sessionService.getSession();
        adminUserEndpoint.createUserWithEmail(session, login, password, email);
    }

    @NotNull
    @Override
    public String name() {
        return "user-registry";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
