package ru.volkova.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Task;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import ru.volkova.tm.util.TerminalUtil;

import java.util.List;

@Component
public class TaskListListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show all tasks";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[TASK LIST]");
        @Nullable final String sort = TerminalUtil.nextLine();
        List<Task> tasks = taskEndpoint.findAllTasks(session);
        int index = 1;
        for (final Task task : tasks) {
            System.out.println((index + ". " + task));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "task-show-list";
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
