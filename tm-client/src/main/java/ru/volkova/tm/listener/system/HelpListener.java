package ru.volkova.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.listener.AbstractListener;

public class HelpListener extends AbstractListener {

    @NotNull
    @Autowired
    private AbstractListener[] listeners;

    @Nullable
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "show terminal commands";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[HELP]");
        for (final AbstractListener listener : listeners)
            System.out.println(listener.name() + ": " + listener.description());
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

}
