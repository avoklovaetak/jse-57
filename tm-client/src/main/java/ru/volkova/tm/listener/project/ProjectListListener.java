package ru.volkova.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volkova.tm.endpoint.Session;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.endpoint.Project;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.exception.entity.ObjectNotFoundException;
import java.util.List;

@Component
public class ProjectListListener extends AbstractProjectListener {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "show project list";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectListListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new ObjectNotFoundException();
        System.out.println("[PROJECT LIST]");
        @Nullable final Session session = sessionService.getSession();
        List<Project> projects = projectEndpoint.findAllProjects(session);
        int index = 1;
        for (final Project project : projects) {
            System.out.println((index + ". " + project));
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
