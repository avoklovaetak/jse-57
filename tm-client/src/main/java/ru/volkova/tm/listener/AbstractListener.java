package ru.volkova.tm.listener;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.volkova.tm.endpoint.Role;
import ru.volkova.tm.event.ConsoleEvent;
import ru.volkova.tm.service.SessionService;

@NoArgsConstructor
public abstract class AbstractListener {
    
    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String description();

    public abstract void handler(@NotNull ConsoleEvent event);

    @Nullable
    public abstract String name();

    @Nullable
    public Role[] roles() {
        return null;
    }

    @Nullable
    @Autowired
    protected SessionService sessionService;

}
