package ru.volkova.tm.configuration;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.volkova.tm")
public class LoggerConfiguration {

}