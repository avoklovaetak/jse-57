package ru.volkova.tm;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.volkova.tm.api.service.IActiveMQConnectionService;
import ru.volkova.tm.configuration.LoggerConfiguration;
import ru.volkova.tm.listener.LogMessageListener;
import ru.volkova.tm.service.ActiveMQConnectionService;

public class Application {

    @SneakyThrows
    public static void main(final String[] args) {
        @NotNull AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final IActiveMQConnectionService receiverService = context.getBean(ActiveMQConnectionService.class);
        receiverService.receive(context.getBean(LogMessageListener.class));
    }

}
